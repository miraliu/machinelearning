import sFrame
import numpy as np

def get_numpy_data(data_sframe, features, output):
    data_sframe['constant'] = 1 # this is how you add a constant column to an SFrame
    # add the column 'constant' to the front of the features list so that we can extract it along with the others:
    features = ['constant'] + features # this is how you combine two lists
    # select the columns of data_SFrame given by the features list into the SFrame features_sframe (now including constant):
    features_sframe = data_sframe[features]
    # the following line will convert the features_SFrame into a numpy matrix:
    feature_matrix = features_sframe.to_numpy()
    # assign the column of data_sframe associated with the output to the SArray output_sarray
    output_sarray = data_sframe[output]
    # the following will convert the SArray into a numpy array by first converting it to a list
    output_array = output_sarray.to_numpy()
    return(feature_matrix, output_array)

def predict_output(feature_matrix, weights):
    # assume feature_matrix is a numpy matrix containing the features as columns and weights is a corresponding numpy array
    # create the predictions vector by using np.dot()
    return(np.dot(feature_matrix, weights))

def feature_derivative(errors, feature):
    # Assume that errors and feature are both numpy arrays of the same length (number of data points)
    # compute twice the dot product of these vectors as 'derivative' and return the value
    return(2*np.dot(errors, feature))

def regression_gradient_descent(feature_matrix, output, initial_weights, step_size, tolerance):
    converged = False
    weights = np.array(initial_weights) # make sure it's a numpy array
    while not converged:
        # compute the predictions based on feature_matrix and weights using your predict_output() function
        predictions = predict_output(feature_matrix, weights)
        # compute the errors as predictions - output
        errors = predictions - output
        gradient_sum_squares = 0 # initialize the gradient sum of squares
        # while we haven't reached the tolerance yet, update each feature's weight
        for i in range(len(weights)): # loop over each weight
            # Recall that feature_matrix[:, i] is the feature column associated with weights[i]
            # compute the derivative for weight[i]:
            derivative = feature_derivative(errors, feature_matrix[:, i])
            # add the squared value of the derivative to the gradient magnitude (for assessing convergence)
            gradient_sum_squares += derivative * derivative
            # subtract the step size times the derivative from the current weight
            weights[i] -= step_size * derivative
        # compute the square-root of the gradient sum of squares to get the gradient matnigude:
        gradient_magnitude = sqrt(gradient_sum_squares)
        if gradient_magnitude < tolerance:
            converged = True
    return(weights)

def get_rss(predictions, outcome):
    # First get the predictions
    RSS = float(0.0000)
    # Then compute the residuals/errors
    for i in range(0,len(outcome)):
        RSS += (outcome[i] - predictions[i]) * (outcome[i] - predictions[i])
    # Then square and add them up
    return(RSS)

def feature_derivative_ridge(errors, feature, weight, l2_penalty, feature_is_constant):
    # If feature_is_constant is True, derivative is twice the dot product of errors and feature
    if feature_is_constant:
        return(2*np.dot(errors, feature))
    # Otherwise, derivative is twice the dot product plus 2*l2_penalty*weight
    else:
        return 2*np.dot(errors, feature) + 2*l2_penalty*weight

def ridge_regression_gradient_descent(feature_matrix, output, initial_weights, step_size, l2_penalty, max_iterations=100):
    weights = np.array(initial_weights) # make sure it's a numpy array
    iteration = 0
    while iteration < max_iterations:
    #while not reached maximum number of iterations:
        # compute the predictions based on feature_matrix and weights using your predict_output() function
        predictions = predict_output(feature_matrix, weights)
        # compute the errors as predictions - output
        errors = predictions - output
        for idx in range(0,len(weights)): # loop over each weight
            # Recall that feature_matrix[:,i] is the feature column associated with weights[i]
            # compute the derivative for weight[i].
            #(Remember: when i=0, you are computing the derivative of the constant!)
            if idx == 0:
                derivative = feature_derivative_ridge(errors, feature_matrix[:,idx], weights[idx], l2_penalty, True)
            else:
                derivative = feature_derivative_ridge(errors, feature_matrix[:,idx], weights[idx], l2_penalty, False)
            # subtract the step size times the derivative from the current weight
            weights[idx] -= step_size * derivative
        iteration += 1
    return weights

def normalize_features(feature_matrix):
    norms = np.linalg.norm(feature_matrix, axis=0)
    return (feature_matrix/norms, norms)

def lasso_coordinate_descent_step(i, feature_matrix, output, weights, l1_penalty):
    # compute prediction
    prediction = predict_output(feature_matrix, weights)
    # compute ro[i] = SUM[ [feature_i]*(output - prediction + weight[i]*[feature_i]) ]
    ro_i = sum(feature_matrix[:,i] * (output - prediction + weights[i]*feature_matrix[:,i]))
    #        ┌ (ro[i] + lambda/2)     if ro[i] < -lambda/2
    #w[i] = ├ 0                      if -lambda/2 <= ro[i] <= lambda/2
    #       └ (ro[i] - lambda/2)     if ro[i] > lambda/2
    if i == 0: # intercept -- do not regularize
        new_weight_i = ro_i
    elif ro_i < -l1_penalty/2.:
        new_weight_i = ro_i + l1_penalty/2
    elif ro_i > l1_penalty/2.:
        new_weight_i = ro_i - l1_penalty/2
    else:
        new_weight_i = 0.
    return new_weight_i

def lasso_cyclical_coordinate_descent(feature_matrix, output, initial_weights, l1_penalty, tolerance):
    optimal = False
    weights = initial_weights
    while optimal == False:
        changes = []
        for i in range(len(weights)):
            old_weights_i = weights[i] # remember old value of weight[i], as it will be overwritten
            # the following line uses new values for weight[0], weight[1], ..., weight[i-1]
            #     and old values for weight[i], ..., weight[d-1]
            weights[i] = lasso_coordinate_descent_step(i, feature_matrix, output, weights, l1_penalty)
            # use old_weights_i to compute change in coordinate
            changes.append(np.absolute(old_weights_i - initial_weights[i]))
        if max(changes) < tolerance:
            optimal = True
    return weights

def one_distance(x1, x2):
    return np.sqrt(np.sum((x1-x2)**2))

def euclidean_distances(data, query):
    diff = data - query
    distances = np.sqrt(np.sum(diff**2, axis=1))
    return distances

def min_distance(distances):
    return list(distances).index(min(distances)), min(distances)

#K nearest Neighbors
def k_nn(k, data, query):
    distances = list(euclidean_distances(data, query))
    dists = list(distances)
    k_close = []
    for i in range(0,k):
        idx,obj = min_distance(dists)
        dists.remove(obj)
        k_close.append(distances.index(obj))
    return k_close

def k_nn_predict_one(k, data, output, query):
    knn = k_nn(k, data, query)
    sum_y = 0.000
    for idx in knn:
        sum_y += output[idx]
    return sum_y / k

def k_nn_predict_set(k, data, output, query_set):
    qs = query_set
    predictions = []
    for i in range(0,len(query_set)):
        predictions.append(k_nn_predict_one(k, data, output, qs[i]))
    return predictions

